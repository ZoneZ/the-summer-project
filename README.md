# Mentoria Indonesia API

[![pipeline status](https://gitlab.com/mentoria-indonesia-tech-team/mentoria-indonesia-api/badges/master/pipeline.svg)](https://gitlab.com/mentoria-indonesia-tech-team/mentoria-indonesia-api/commits/master)
[![coverage report](https://gitlab.com/mentoria-indonesia-tech-team/mentoria-indonesia-api/badges/master/coverage.svg)](https://gitlab.com/mentoria-indonesia-tech-team/mentoria-indonesia-api/commits/master)

* [Description](#description)
* [Installation](#installation-for-development)
* [Usage](#usage)
* [License](#license)

## Description

Private API for Mentoria Indonesia web application. Handles most of business logics such as user authentication and
event booking processes. Made using Django REST Framework.

## Installation (For Development)
1. Make sure you are working in a Python 3 virtual environment. In case you haven't created one, run one of these 
commands:
    ```
    python3 -m venv /path/to/new/virtual/environment/<venv_name>
    ```
   or
    ```
    python -m venv /path/to/new/virtual/environment/<venv_name>
    ```
    depending on which command runs Python 3 on your machine.
2. Activate the virtual environment on your terminal.
    * Posix (ex: Linux)
      ```
      $ source <venv_name>/bin/activate
      ```
    * Windows
      ```
      <venv_name>/Scripts/activate.bat
      ```
3. Once you have activated the virtual environment, install all the required dependencies.
   ```
   pip install -r requirements/development.txt
   ```
4. Create a ```.env``` file on the same directory level as ```manage.py``` module. 
Follow one ```.env.<environment_name>.sample``` file included in this
repo to see which environment variables should be set.

## Usage
Make sure you have followed the steps listed on Installation section.
### Running the Project
To run the project, run the command below:
```
python manage.py runserver 8000 --settings=mentoria_api.settings.<environment_name>
```
Replace <environment_name> with one of the following:

- development
- staging

### Running the Tests
To run the tests, run the command below:
```
python manage.py test --settings=mentoria_api.settings.test
```

## License

```
Copyright 2019, Mentoria Indonesia Tech Team
```
