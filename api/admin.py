import json

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib import admin
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Count
from django.db.models.functions import TruncDay
from django.http import JsonResponse
from django.urls import path, reverse
from django.utils.safestring import mark_safe
from django_summernote.admin import SummernoteModelAdmin

from api.models import (APIUser, EventInfo, EventRating, EventSlot, Location,
                        UserEvent, UserEventBooking, BlogArticle, BlogCategory, BlogTag)

admin.site.site_header = admin.site.site_title = 'Mentoria Indonesia API Administration'
admin.site.register(Location)


def get_object_change_display(related_object):
    display_text = "<a href={url}>{name}</a>".format(
        url=reverse(
            'admin:{}_{}_change'.format(
                related_object._meta.app_label,
                related_object._meta.model_name
            ),
            args=(related_object.pk,)
        ),
        name=related_object.__str__()
    )
    if display_text:
        return mark_safe(display_text)
    return "-"


@admin.register(APIUser)
class APIUserAdmin(admin.ModelAdmin):
    search_fields = ('username', 'first_name', 'last_name', 'email')
    list_display = ('username', 'first_name', 'last_name', 'is_email_verified')
    list_filter = ('is_email_verified',)


def make_user_booking_complete(modeladmin, request, queryset):
    queryset.update(status='COMPLETE')


make_user_booking_complete.short_description = "Mark selected event bookings as complete"


class LocationInline(admin.TabularInline):
    model = EventInfo.locations.through


class EventSlotInlineFormSet(forms.BaseInlineFormSet):

    def clean(self):
        super(EventSlotInlineFormSet, self).clean()

        if any(self.errors):
            return

        one_active_slot = False
        for form in self.forms:
            slot_is_active = form.cleaned_data.get('is_active')
            if not one_active_slot:
                one_active_slot = True
                continue
            if one_active_slot and slot_is_active:
                raise forms.ValidationError("There can only be one active slot for this event.")


class EventSlotInline(admin.TabularInline):
    model = EventSlot
    formset = EventSlotInlineFormSet


class EventRatingInline(admin.TabularInline):
    model = EventRating

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(EventInfo)
class EventInfoAdmin(SummernoteModelAdmin):
    readonly_fields = ('slug',)
    summernote_fields = ('description',)
    inlines = [
        LocationInline,
        EventSlotInline,
        EventRatingInline,
    ]
    search_fields = ('name', 'price', 'locations__location_name')
    list_display = ('name', 'start_at', 'end_at')
    list_filter = ('start_at', 'end_at', 'price', 'locations')

    def get_urls(self):
        urls = super(EventInfoAdmin, self).get_urls()
        extra_urls = [
            path(
                '<path:object_id>/change/chart_data/',
                self.admin_site.admin_view(self.chart_data_endpoint)
            )
        ]
        return extra_urls + urls

    def chart_data_endpoint(self, request, object_id):
        chart_data = self.chart_data(object_id)
        return JsonResponse(list(chart_data), safe=False)

    @staticmethod
    def chart_data(object_id):
        return (
            UserEventBooking.objects.filter(user_event__event_id=object_id)
                .annotate(date=TruncDay('created'))
                .values('date')
                .annotate(y=Count('id'))
                .order_by('-date')
        )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        chart_data = self.chart_data(object_id=object_id)
        chart_data_as_json = json.dumps(list(chart_data), cls=DjangoJSONEncoder)
        extra_context = extra_context or {'chart_data': chart_data_as_json, 'object_id': object_id}

        return super(EventInfoAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


@admin.register(UserEvent)
class UserEventAdmin(admin.ModelAdmin):
    readonly_fields = ('user', 'event')
    search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email')
    list_display = ('__str__', 'event_display', 'user_display')
    list_filter = ('event',)

    def event_display(self, obj):
        return get_object_change_display(obj.event)

    event_display.short_description = 'Event'

    def user_display(self, obj):
        return get_object_change_display(obj.user)

    user_display.short_description = 'User'


class UserEventBookingAdminForm(forms.ModelForm):
    class Meta:
        model = UserEventBooking
        fields = ('user_event', 'event_slot', 'booking_expired_at',
                  'status', 'payment_confirmation_image')

    def clean_status(self):
        status = self.cleaned_data['status']
        booking = self.instance

        if status == UserEventBooking.PENDING or \
                status == UserEventBooking.COMPLETE:
            if not booking.payment_confirmation_image:
                raise forms.ValidationError('Cannot set booking status '
                                            'due to lack of payment confirmation image!')
        return status


@admin.register(UserEventBooking)
class UserEventBookingAdmin(admin.ModelAdmin):
    readonly_fields = ('user_event', 'event_slot', 'booking_expired_at')
    actions = [make_user_booking_complete]
    search_fields = ('user_event__user__first_name', 'user_event__user__last_name',
                     'user_event__event__name')
    form = UserEventBookingAdminForm
    list_display = ('id', 'event_display', 'user_display', 'booking_expired_at', 'status_display')
    list_filter = ('user_event__event', 'status', 'created', 'booking_expired_at')

    def event_display(self, obj):
        return get_object_change_display(obj.user_event.event)

    event_display.short_description = 'Event'

    def user_display(self, obj):
        return get_object_change_display(obj.user_event.user)

    user_display.short_description = 'User'

    def status_display(self, obj):
        booking_status = obj.status
        if booking_status == UserEventBooking.NOT_COMPLETE:
            display_text = "<p style='color: red;'>NOT COMPLETE</p>"
        elif booking_status == UserEventBooking.PENDING:
            display_text = "<p style='color: orange;'>WAITING FOR CONFIRMATION</p>"
        else:
            display_text = "<p style='color: green;'>COMPLETE</p>"

        return mark_safe(display_text)

    status_display.short_description = 'Status'


class BlogArticleAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = BlogArticle
        fields = ('title', 'headline', 'cover_image', 'content',
                  'tags', 'categories')


@admin.register(BlogArticle)
class BlogArticleAdmin(admin.ModelAdmin):
    form = BlogArticleAdminForm
    readonly_fields = ('author',)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.author = request.user
        super(BlogArticleAdmin, self).save_model(request, obj, form, change)


@admin.register(BlogTag)
class BlogTagAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(BlogCategory)
class BlogCategoryAdmin(admin.ModelAdmin):
    search_fields = ('name', 'description')
    list_display = ('__str__', 'description')
