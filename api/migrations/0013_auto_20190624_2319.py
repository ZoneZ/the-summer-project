# Generated by Django 2.2.2 on 2019-06-24 16:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20190624_2308'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventslot',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='This slot is active for this event'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usereventbooking',
            name='event_slot',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to='api.EventSlot'),
            preserve_default=False,
        ),
    ]
