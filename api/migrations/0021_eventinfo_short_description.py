# Generated by Django 2.2.2 on 2019-07-09 08:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0020_auto_20190707_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventinfo',
            name='short_description',
            field=models.CharField(default='ancol', max_length=200),
            preserve_default=False,
        ),
    ]
