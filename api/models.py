import random
import string
from datetime import timedelta

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import UniqueConstraint
from django.utils import timezone
from django.utils.text import slugify

from api.signals import booking_confirmed


class TimeStampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def generate_slug(self, attribute):
        assert hasattr(self, 'slug'), 'This model instance does not have slug attribute.'
        slug = slugify(attribute)
        if self.__class__.objects.filter(slug=slug).exclude(id=self.id).exists():
            slug = "{slug}-{randomstr}".format(
                slug=slug,
                randomstr=''.join(
                    random.choice(string.ascii_lowercase + string.digits)
                    for _ in range(8)
                )
            )
        return slug


class APIUser(AbstractUser):
    birth_date = models.DateField(null=True)
    phone_number = models.CharField(max_length=15, null=True, blank=True, default='')
    line_id = models.CharField(max_length=50, null=True, blank=True, default='')
    location = models.CharField(max_length=100, null=True, blank=True, default='')
    education = models.CharField(max_length=15, null=True, blank=True, default='')
    institution = models.CharField(max_length=50, null=True, blank=True, default='')
    is_email_verified = models.BooleanField(
        verbose_name="User's email is verified",
        default=False
    )
    is_premium = models.BooleanField(
        verbose_name="User's account type is premium",
        default=False
    )
    profile_picture = models.ImageField(
        verbose_name="User's profile picture",
        upload_to='user_profile_picture',
        null=True,
        blank=True
    )
    about = models.TextField(
        verbose_name="User's additional informations",
        null=True,
        blank=True
    )

    @property
    def name(self):
        if self.first_name or self.last_name:
            return self.first_name + " " + self.last_name
        return self.username

    # TODO find a more elegant and sustainable approach
    @property
    def is_profile_complete(self):
        return all([self.birth_date, self.phone_number, self.line_id,
                    self.location, self.education, self.institution])


class Location(TimeStampModel):
    location_name = models.CharField(max_length=100)
    address = models.TextField()
    map_url = models.URLField(verbose_name="Google Maps URL for the location")

    def __str__(self):
        return self.location_name


class EventInfo(TimeStampModel):
    name = models.CharField(max_length=100)
    cover_image = models.URLField(null=True)
    slug = models.SlugField(max_length=100, null=True)
    short_description = models.CharField(max_length=200)
    description = models.TextField()
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()
    price = models.DecimalField(max_digits=19, decimal_places=5)
    locations = models.ManyToManyField(
        to=Location,
        related_name='events',
        through='EventLocation'
    )

    @property
    def booking_deadline(self):
        return (timezone.localtime(self.start_at) - timedelta(days=1)).replace(hour=23, minute=59)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.start_at >= self.end_at:
            raise ValidationError("Start time cannot be after end time.")
        self.slug = self.generate_slug(self.name)
        super(EventInfo, self).save(*args, **kwargs)


class EventLocation(TimeStampModel):
    event = models.ForeignKey(to=EventInfo, on_delete=models.CASCADE)
    location = models.ForeignKey(to=Location, on_delete=models.CASCADE)
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()
    info = models.CharField(max_length=30)

    def __str__(self):
        return "Event: {} - Location: {}".format(self.event.name, self.location.location_name)


class EventSlot(TimeStampModel):
    event = models.ForeignKey(to=EventInfo, on_delete=models.CASCADE)
    slot = models.IntegerField(validators=[MinValueValidator(limit_value=0)])
    payment_limit_hour = models.IntegerField(
        verbose_name="Payment limit time (in hour)",
        validators=[
            MinValueValidator(limit_value=0),
            MaxValueValidator(limit_value=1000)
        ]
    )
    is_active = models.BooleanField(verbose_name="This slot is active for this event")

    @property
    def available_slots(self):
        return self._get_taken_slots()

    def _get_taken_slots(self):
        available_slots = self.slot
        incomplete_bookings = self._get_incomplete_bookings()
        pending_bookings = self._get_pending_bookings()
        completed_bookings = self._get_complete_bookings()

        for booking in incomplete_bookings.iterator():
            if not booking.is_payment_expired:
                available_slots -= 1

        available_slots -= completed_bookings.count() + pending_bookings.count()
        return available_slots

    def _get_complete_bookings(self):
        return UserEventBooking.objects.get_complete_bookings_from_slot(self.id)

    def _get_pending_bookings(self):
        return UserEventBooking.objects.get_pending_bookings_from_slot(self.id)

    def _get_incomplete_bookings(self):
        return UserEventBooking.objects.get_incomplete_bookings_from_slot(self.id)

    def __str__(self):
        return "[{}] Event: {}  - Slot: {} ".format("ACTIVE" if self.is_active else "INACTIVE",
                                                    self.event, self.slot)

    def save(self, *args, **kwargs):
        if EventSlot.objects \
                .filter(event_id=self.event_id, is_active=True) \
                .exclude(id=self.id) \
                .exists() \
                and \
                self.is_active:
            raise ValidationError("An event cannot have more than one active slot.")
        super(EventSlot, self).save(*args, **kwargs)


class EventRating(TimeStampModel):
    user = models.ForeignKey(to=APIUser, on_delete=models.CASCADE)
    event = models.ForeignKey(to=EventInfo, on_delete=models.CASCADE)
    rating = models.IntegerField(
        validators=[
            MinValueValidator(limit_value=1),
            MaxValueValidator(limit_value=5)
        ]
    )
    review = models.TextField()

    class Meta:
        constraints = [
            UniqueConstraint(fields=['user', 'event'], name='unique_rating')
        ]

    def __str__(self):
        return "Event: {} - User: {}".format(self.event.name, self.user.name)


class UserEvent(TimeStampModel):
    user = models.ForeignKey(to=APIUser, on_delete=models.CASCADE)
    event = models.ForeignKey(to=EventInfo, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=['user', 'event'],
                name='unique_user_event'
            )
        ]

    def __str__(self):
        return "Event: {} - User: {}".format(self.event.name, self.user.name)


class EventBookingManager(models.Manager):

    def get_complete_bookings_from_slot(self, slot_id):
        return self.filter(event_slot_id=slot_id, status=UserEventBooking.COMPLETE)

    def get_incomplete_bookings_from_slot(self, slot_id):
        return self.filter(event_slot_id=slot_id, status=UserEventBooking.NOT_COMPLETE)

    def get_pending_bookings_from_slot(self, slot_id):
        return self.filter(event_slot_id=slot_id, status=UserEventBooking.PENDING)


class UserEventBooking(TimeStampModel):
    NOT_COMPLETE = 'NOT_COMPLETE'
    PENDING = 'PENDING'
    COMPLETE = 'COMPLETE'
    STATUS_CHOICES = [
        (NOT_COMPLETE, 'Booking process is not complete'),
        (PENDING, 'Booking is waiting for admin confirmation'),
        (COMPLETE, 'Booking process is complete')
    ]

    user_event = models.ForeignKey(to=UserEvent, on_delete=models.CASCADE)
    event_slot = models.ForeignKey(to=EventSlot, on_delete=models.CASCADE)
    booking_expired_at = models.DateTimeField(null=True, blank=True)
    status = models.CharField(
        max_length=15,
        choices=STATUS_CHOICES,
        default=NOT_COMPLETE
    )
    payment_confirmation_image = models.ImageField(
        verbose_name="Image/photo of payment for the booking",
        upload_to='user_event_payment',
        null=True,
        blank=True
    )

    objects = EventBookingManager()

    def __str__(self):
        return "{} - Payment complete: {}".format(self.user_event, self.status)

    @property
    def is_payment_expired(self):
        return timezone.localtime(timezone.now()) >= timezone.localtime(self.booking_expired_at)

    def save(self, *args, **kwargs):
        if not self.booking_expired_at:
            event_booking_deadline = self.user_event.event.booking_deadline
            payment_limit_hour = self.event_slot.payment_limit_hour
            normal_payment_deadline = timezone.localtime(timezone.now()) + timedelta(hours=payment_limit_hour)
            if normal_payment_deadline >= event_booking_deadline:
                self.booking_expired_at = event_booking_deadline
            else:
                self.booking_expired_at = normal_payment_deadline

        if self.status == self.COMPLETE:
            booking_confirmed.send_robust(
                sender=self.__class__,
                user=self.user_event.user,
                booking=self
            )
        super(UserEventBooking, self).save(*args, **kwargs)


class BlogTag(TimeStampModel):
    name = models.CharField(max_length=25, unique=True)
    slug = models.CharField(max_length=33, null=True, blank=True)

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.slug = self.generate_slug(self.name)
        super(BlogTag, self).save(*args, **kwargs)


class BlogCategory(TimeStampModel):
    name = models.CharField(max_length=25, unique=True)
    description = models.CharField(max_length=100)
    slug = models.CharField(max_length=33, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Blog categories'

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.slug = self.generate_slug(self.name)
        super(BlogCategory, self).save(*args, **kwargs)


class BlogArticle(TimeStampModel):
    title = models.CharField(max_length=100)
    headline = models.CharField(max_length=50)
    cover_image = models.ImageField(
        upload_to='blog_article_cover_image',
        null=True,
        blank=True
    )
    content = models.TextField()
    slug = models.CharField(max_length=108, null=True, blank=True)
    author = models.ForeignKey(to=APIUser, on_delete=models.CASCADE)
    tags = models.ManyToManyField(to=BlogTag)
    categories = models.ManyToManyField(to=BlogCategory)

    class Meta:
        ordering = ('-created',)

    def save(self, *args, **kwargs):
        self.slug = self.generate_slug(self.title)
        super(BlogArticle, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
