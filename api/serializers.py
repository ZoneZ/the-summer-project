from allauth.socialaccount.models import SocialAccount
from django.utils import timezone
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import (APIUser, EventInfo, EventSlot, Location,
                     UserEvent, UserEventBooking, BlogCategory, BlogTag, BlogArticle)


class APIUserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='users-detail',
        lookup_field='pk',
    )
    email = serializers.EmailField(validators=[UniqueValidator(
        queryset=APIUser.objects.all(),
        message='This email has already been registered.')]
    )

    class Meta:
        model = APIUser
        fields = ('url', 'username', 'email', 'password', 'birth_date', 'phone_number', 'line_id',
                  'location', 'education', 'institution', 'first_name', 'last_name', 'is_email_verified')
        extra_kwargs = {'password': {'write_only': True}}

    def to_representation(self, instance):
        representation = super(APIUserSerializer, self).to_representation(instance)
        representation['user_id'] = instance.id
        representation['name'] = instance.name
        representation['is_signed_with_social_account'] = SocialAccount.objects.filter(user=instance).exists()
        representation['is_profile_complete'] = instance.is_profile_complete
        return representation

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = APIUser(**validated_data)
        user.set_password(password)
        user.save()
        return user


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('location_name', 'address', 'map_url')


class EventInfoSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='event-detail',
        lookup_field='slug',
    )
    locations = LocationSerializer(many=True, read_only=True)

    class Meta:
        model = EventInfo
        fields = ('url', 'id', 'name', 'slug', 'cover_image', 'short_description',
                  'description', 'start_at', 'end_at', 'price', 'locations')


class EventSlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventSlot
        fields = ('id', 'event', 'slot', 'payment_limit_hour', 'is_active')

    def to_representation(self, instance):
        representation = super(EventSlotSerializer, self).to_representation(instance)
        representation['available_slots'] = instance.available_slots
        return representation


class UserEventBookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserEventBooking
        fields = ('id', 'user_event', 'event_slot', 'created', 'booking_expired_at',
                  'status', 'payment_confirmation_image')

    def validate(self, data):
        user_event = UserEvent.objects.select_related('event').get(id=data['user_event'].id)
        event_slot = EventSlot.objects.select_related('event').get(id=data['event_slot'].id)
        if user_event.event.id != event_slot.event.id:
            raise serializers.ValidationError(detail='User event and event slot does not match!')

        event = user_event.event
        if timezone.localtime(timezone.now()) >= timezone.localtime(event.start_at):
            raise serializers.ValidationError(detail='Event has already started.')

        return data

    def update(self, instance, validated_data):
        booking = super(UserEventBookingSerializer, self).update(instance, validated_data)
        if booking.payment_confirmation_image:
            booking.status = booking.PENDING
            booking.save()
        return booking


class UserEventSerializer(serializers.ModelSerializer):
    event = EventInfoSerializer()
    bookings = UserEventBookingSerializer(source='usereventbooking_set', many=True, read_only=True)

    class Meta:
        model = UserEvent
        fields = ('id', 'user', 'event', 'bookings',)

    def create(self, validated_data):
        event = EventInfo.objects.create(**validated_data['event'])
        user = APIUser.objects.get(username=validated_data['user'])
        user_event = UserEvent.objects.create(user=user,
                                              event=event)
        return user_event


class BlogTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogTag
        fields = ('name', 'slug')


class BlogCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogCategory
        fields = ('name', 'description', 'slug')


class BlogArticleSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='blog-articles-detail',
        lookup_field='slug'
    )
    author = APIUserSerializer()
    tags = BlogTagSerializer(many=True, read_only=True, required=False)
    categories = BlogCategorySerializer(many=True, read_only=True, required=False)

    class Meta:
        model = BlogArticle
        fields = ('url', 'created', 'title', 'headline', 'cover_image', 'content', 'slug',
                  'author', 'tags', 'categories')

    def create(self, validated_data):
        author_data = validated_data.pop('author')
        author = APIUser.objects.create(**author_data)
        article = BlogArticle.objects.create(author=author, **validated_data)
        return article

    def to_representation(self, instance):
        representation = super(BlogArticleSerializer, self).to_representation(instance)
        author = instance.author
        representation['author'] = {
            'name': author.name,
            'profile_picture': author.profile_picture.url if author.profile_picture else None,
            'about': author.about
        }
        return representation
