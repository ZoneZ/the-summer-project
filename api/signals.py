import django.dispatch
from django.conf import settings
from django.dispatch import receiver
from django.template.loader import render_to_string

from api.utils import EmailBuilder

booking_created = django.dispatch.Signal(providing_args=['user', 'booking'])
booking_payment_uploaded = django.dispatch.Signal(providing_args=['user', 'booking'])
booking_confirmed = django.dispatch.Signal(providing_args=['user', 'booking'])


@receiver(booking_created)
def successful_booking_send_email(sender, user, booking, **kwargs):
    context = {
        'user': user,
        'booking': booking,
        'event_url': f"{settings.CLIENT_DOMAIN}/events/{booking.user_event.event.slug}/",
        'user_event_url': f"{settings.CLIENT_DOMAIN}/users/my-event/{booking.user_event.id}/"
    }

    email_html_message = render_to_string('email/booking_successful.html', context=context)
    email_plaintext_message = render_to_string('email/booking_successful.txt', context=context)

    message = EmailBuilder(). \
        with_subject(f"Mentoria Indonesia Event - "
                     f"{booking.user_event.event.name} - "
                     f"Selesaikan Proses Pembayaran").\
        with_body(email_plaintext_message).\
        with_from_address('NoReply@mentoria.id').\
        with_to_addresses([user.email]).\
        with_alternative(content=email_html_message, mimetype='text/html')
    message.send()


@receiver(booking_payment_uploaded)
def payment_upload_send_email(sender, user, booking, **kwargs):
    context = {
        'user': user,
        'booking': booking,
        'event_url': f"{settings.CLIENT_DOMAIN}/events/{booking.user_event.event.slug}/"
    }
    email_html_message = render_to_string('email/booking_payment_uploaded.html', context=context)
    email_plaintext_message = render_to_string('email/booking_payment_uploaded.txt', context=context)
    message = EmailBuilder().\
        with_subject(f"Mentoria Indonesia Event - "
                     f"{booking.user_event.event.name} - "
                     f"Bukti Pembayaran Diterima").\
        with_body(email_plaintext_message).\
        with_from_address('NoReply@mentoria.id').\
        with_to_addresses([user.email]).\
        with_alternative(content=email_html_message, mimetype='text/html')
    message.send()


@receiver(booking_confirmed)
def confirmed_booking_send_email(sender, user, booking, **kwargs):
    context = {
        'user': user,
        'booking': booking,
        'event_url': f"{settings.CLIENT_DOMAIN}/events/{booking.user_event.event.slug}/"
    }
    email_html_message = render_to_string('email/booking_confirmed.html', context=context)
    email_plaintext_message = render_to_string('email/booking_confirmed.txt', context=context)
    message = EmailBuilder().\
        with_subject(f"Mentoria Indonesia Event - "
                     f"{booking.user_event.event.name} - "
                     f"Booking Dikonfirmasi").\
        with_body(email_plaintext_message).\
        with_from_address('NoReply@mentoria.id').\
        with_to_addresses([user.email]).\
        with_alternative(content=email_html_message, mimetype='text/html')
    message.send()
