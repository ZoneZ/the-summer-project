from django import template

register = template.Library()


@register.filter(name='indo_localize_date')
def indo_localize_date(value):

    def format_day_name(day):
        days_locale = {
            'Monday': 'Senin',
            'Tuesday': 'Selasa',
            'Wednesday': 'Rabu',
            'Thursday': 'Kamis',
            'Friday': 'Jumat',
            'Saturday': 'Sabtu',
            'Sunday': 'Minggu'
        }
        return days_locale[day]

    def format_month_name(month):
        months_locale = {
            'January': 'Januari',
            'February': 'Februari',
            'March': 'Maret',
            'May': 'Mei',
            'June': 'Juni',
            'July': 'Juli',
            'August': 'Agustus',
            'October': 'Oktober',
            'December': 'Desember'
        }
        return months_locale[month] if month in months_locale.keys() else month

    return f"{format_day_name(value.strftime('%A'))}, " \
           f"{value.day} {format_month_name(value.strftime('%B'))} {value.year}"
