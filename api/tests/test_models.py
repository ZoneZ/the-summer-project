from datetime import date, datetime
from unittest import mock

import pytz
from django.core import mail
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from api.models import APIUser, EventInfo, EventSlot, UserEvent, UserEventBooking, Location, EventLocation, BlogArticle
from api.signals import booking_confirmed
from api.tests.utils import CatchSignal


class APIUserTests(TestCase):

    def test_name_property_with_existing_first_name_and_last_name(self):
        user = APIUser.objects.create(username='dummy', password='secret',
                                      first_name='Mr.', last_name='Dummy')

        self.assertEquals(user.name, 'Mr. Dummy')

    def test_name_property_with_nonexisting_first_name_and_last_name(self):
        user = APIUser.objects.create(username='dummy', password='secret')

        self.assertEquals(user.name, "dummy")

    def test_is_profile_complete_property_with_complete_profile_attributes(self):
        user = APIUser.objects.create(username='dummy', password='secret',
                                      birth_date=date(year=1999, month=8, day=21),
                                      phone_number='123456789', line_id='LINEID',
                                      location='Anywhere', education='Pro',
                                      institution='Anyplace')

        self.assertEquals(user.is_profile_complete, True)

    def test_is_profile_complete_property_with_incomplete_profile_attributes(self):
        user = APIUser.objects.create(username='dummy', password='secret',
                                      birth_date=date(year=1999, month=8, day=21),
                                      education='Pro')

        self.assertEquals(user.is_profile_complete, False)


class EventInfoTests(TestCase):
    local_timezone = pytz.timezone('Asia/Jakarta')

    def test_event_start_datetime_should_be_before_end_datetime(self):
        with self.assertRaises(ValidationError):
            EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                     start_at=datetime(year=2019, month=7, day=15, hour=23, tzinfo=self.local_timezone),
                                     end_at=datetime(year=2019, month=7, day=14, hour=10, tzinfo=self.local_timezone),
                                     price=10000)

    def test_event_info_has_correct_booking_deadline(self):
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        booking_deadline = event.booking_deadline

        self.assertEquals(booking_deadline.year, 2019)
        self.assertEquals(booking_deadline.month, 7)
        self.assertEquals(booking_deadline.day, 14)
        self.assertEquals(booking_deadline.hour, 23)
        self.assertEquals(booking_deadline.minute, 59)

    def test_events_with_different_name_should_have_different_slugs(self):
        event1 = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                          start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                            tzinfo=self.local_timezone),
                                          end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                          tzinfo=self.local_timezone),
                                          price=10000)
        event2 = EventInfo.objects.create(name='eventzzz', short_description='desc', description='desc long',
                                          start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                            tzinfo=self.local_timezone),
                                          end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                          tzinfo=self.local_timezone),
                                          price=10000)

        self.assertNotEquals(event1.slug, event2.slug)

    def test_events_with_same_name_should_have_different_slugs(self):
        event1 = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                          start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                            tzinfo=self.local_timezone),
                                          end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                          tzinfo=self.local_timezone),
                                          price=10000)
        event2 = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                          start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                            tzinfo=self.local_timezone),
                                          end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                          tzinfo=self.local_timezone),
                                          price=10000)

        self.assertNotEquals(event1.slug, event2.slug)


class EventLocationTests(TestCase):

    def setUp(self):
        self.event = EventInfo.objects.create(
            name='event',
            short_description='short desc',
            description='long desc',
            start_at=datetime(2019, 8, 14, 12),
            end_at=datetime(2019, 8, 14, 17),
            price=10000
        )
        self.location = Location.objects.create(
            location_name='New Asgard',
            address='Tonnsberg',
            map_url='https://google.com'
        )

    def test_event_location_object_has_correct_event_and_location_objects(self):
        event_location = EventLocation.objects.create(
            event=self.event,
            location=self.location,
            start_at=self.event.start_at,
            end_at=self.event.end_at,
            info='Main Event'
        )
        self.assertEquals(event_location.event, self.event)
        self.assertEquals(event_location.location, self.location)


class EventSlotTests(TestCase):

    def setUp(self):
        self.user = APIUser.objects.create(
            username='dummy',
            password='secret'
        )
        self.event = EventInfo.objects.create(
            name='dummyevent', short_description='desc',
            description='desc long',
            start_at=datetime(2019, 7, 15, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 16, 10, tzinfo=pytz.utc),
            price=10000
        )

    def test_an_event_should_not_have_more_than_one_active_slot(self):
        with self.assertRaises(ValidationError):
            EventSlot.objects.create(event=self.event, slot=10, payment_limit_hour=1, is_active=True)
            EventSlot.objects.create(event=self.event, slot=20, payment_limit_hour=2, is_active=True)

    def test_available_slots_property_value_should_be_less_than_initial_slot_when_there_exists_incomplete_booking(self):
        slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        user_event = UserEvent.objects.create(
            user=self.user,
            event=self.event
        )

        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 14, tzinfo=pytz.utc))):
            UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=slot,
                status=UserEventBooking.NOT_COMPLETE
            )

            self.assertEquals(slot.available_slots, 0)

    def test_available_slots_property_value_should_be_less_than_initial_slot_when_there_exists_pending_booking(self):
        slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        user_event = UserEvent.objects.create(
            user=self.user,
            event=self.event
        )
        UserEventBooking.objects.create(
            user_event=user_event,
            event_slot=slot,
            status=UserEventBooking.PENDING
        )

        self.assertEquals(slot.available_slots, 0)

    def test_available_slots_property_value_should_be_less_than_initial_slot_when_there_exists_complete_booking(self):
        slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        user_event = UserEvent.objects.create(
            user=self.user,
            event=self.event
        )
        UserEventBooking.objects.create(
            user_event=user_event,
            event_slot=slot,
            status=UserEventBooking.COMPLETE
        )

        self.assertEquals(slot.available_slots, 0)


class UserEventBookingTests(TestCase):
    local_timezone = pytz.timezone('Asia/Jakarta')

    def test_is_payment_expired_property(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)

        mocked_datetime = timezone.now()
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot,
                                                      booking_expired_at=timezone.now())

            self.assertEquals(booking.is_payment_expired, True)

    def test_expiration_of_late_booking_is_the_same_as_booking_deadline(self):
        """
        Every event booking deadline: D-1 at 23.59 local time Asia/Jakarta.
        "Late booking" : a booking that once made, its "supposed" payment deadline is ahead of booking deadline.
        """
        user = APIUser.objects.create(username='dummy', password='secret')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)

        mocked_datetime = datetime(year=2019, month=7, day=14, hour=15, tzinfo=self.local_timezone)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)
            booking_expiration = booking.booking_expired_at

            self.assertEquals(booking_expiration, booking.user_event.event.booking_deadline)
            self.assertEquals(booking_expiration.year, 2019)
            self.assertEquals(booking_expiration.month, 7)
            self.assertEquals(booking_expiration.day, 14)
            self.assertEquals(booking_expiration.hour, 23)
            self.assertEquals(booking_expiration.minute, 59)

    def test_expiration_of_punctual_booking_is_according_to_the_slot_payment_limit_hour(self):
        """
        "Punctual booking" : a booking that once made, its "supposed" payment deadline is not ahead of booking deadline.
        """
        user = APIUser.objects.create(username='dummy', password='secret')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)

        mocked_datetime = datetime(year=2019, month=7, day=14, hour=3, tzinfo=self.local_timezone)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)
            booking_expiration = booking.booking_expired_at

            self.assertEquals(booking_expiration.year, 2019)
            self.assertEquals(booking_expiration.month, 7)
            self.assertEquals(booking_expiration.day, 14)
            self.assertEquals(booking_expiration.hour, 13)

    def test_event_booking_initial_status_on_creation_is_not_complete(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)
        eventbooking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)

        self.assertEqual(eventbooking.status, 'NOT_COMPLETE')

    def test_status_changed_to_complete_should_send_signal(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)
        event_booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)

        with CatchSignal(booking_confirmed) as handler:
            event_booking.status = event_booking.COMPLETE
            event_booking.save()

        handler.assert_called_once()

    def test_status_changed_to_complete_should_send_email_to_the_user(self):
        user = APIUser.objects.create(username='dummy', password='secret', email='dummy@example.com')
        event = EventInfo.objects.create(name='dummyevent', short_description='desc', description='desc long',
                                         start_at=datetime(year=2019, month=7, day=15, hour=10,
                                                           tzinfo=self.local_timezone),
                                         end_at=datetime(year=2019, month=7, day=16, hour=10,
                                                         tzinfo=self.local_timezone),
                                         price=10000)
        slot = EventSlot.objects.create(event=event, slot=20, payment_limit_hour=10, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event)
        event_booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)
        event_booking.status = event_booking.COMPLETE
        event_booking.save()

        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to[0], user.email)


class BlogArticleTests(TestCase):

    def setUp(self):
        self.author = APIUser.objects.create(username='Author', password='Pass')

    def test_articles_with_different_title_should_have_different_slugs(self):
        article1 = BlogArticle.objects.create(
            title='Title 1',
            headline='Headline 1',
            content='Content',
            author=self.author
        )
        article2 = BlogArticle.objects.create(
            title='Title 2',
            headline='Headline 2',
            content='Content',
            author=self.author
        )

        self.assertNotEquals(article1.slug, article2.slug)

    def test_articles_with_same_title_should_have_different_slugs(self):
        article1 = BlogArticle.objects.create(
            title='Title 1',
            headline='Headline 1',
            content='Content',
            author=self.author
        )
        article2 = BlogArticle.objects.create(
            title='Title 1',
            headline='Headline 2',
            content='Content',
            author=self.author
        )

        self.assertNotEquals(article1.slug, article2.slug)
