from datetime import datetime
from unittest import mock

import pytz
from django.utils import timezone
from rest_framework.test import APITestCase, APIRequestFactory

from api.models import APIUser, Location, EventInfo, EventSlot, UserEvent, UserEventBooking, BlogArticle, BlogTag, \
    BlogCategory
from api.serializers import APIUserSerializer, LocationSerializer, EventInfoSerializer, EventSlotSerializer, \
    UserEventBookingSerializer, UserEventSerializer, BlogCategorySerializer, BlogTagSerializer, BlogArticleSerializer


class APIUserSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        data = {'username': 'dummy', 'email': 'dummy@mail.com', 'password': 'secret',
                'birth_date': '2019-01-01', 'phone_number': '123456789', 'line_id': 'linetes',
                'location': 'loc', 'education': 'pro', 'institution': 'anywhere',
                'first_name': 'Mr.', 'last_name': 'Dummy'}
        serializer = APIUserSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertCountEqual(serializer.data.keys(),
                              ['url', 'username', 'email', 'birth_date', 'phone_number', 'line_id',
                               'location', 'education', 'institution', 'first_name', 'last_name',
                               'is_email_verified', 'user_id', 'name', 'is_signed_with_social_account',
                               'is_profile_complete'])

    def test_serializer_can_create_api_user_object(self):
        data = {'username': 'dummy', 'email': 'dummy@mail.net', 'password': 'secret'}
        serializer = APIUserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(APIUser.objects.count(), 1)

    def test_serializer_can_serialize_api_user_object_properly(self):
        data = {'username': 'dummy', 'email': 'dummy@mail.net', 'password': 'secret',
                'birth_date': '2019-01-01', 'phone_number': '123', 'line_id': 'linetes',
                'location': 'anywhere', 'education': 'pro', 'institution': 'anywho',
                'first_name': 'Mr.', 'last_name': 'Dummy'}
        serializer = APIUserSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = APIUser.objects.first()

        self.assertEquals(serializer.data['username'], user.username)
        self.assertEquals(serializer.data['email'], user.email)
        self.assertEquals(serializer.data['birth_date'], user.birth_date.isoformat())
        self.assertEquals(serializer.data['phone_number'], user.phone_number)
        self.assertEquals(serializer.data['line_id'], user.line_id)
        self.assertEquals(serializer.data['location'], user.location)
        self.assertEquals(serializer.data['education'], user.education)
        self.assertEquals(serializer.data['institution'], user.institution)
        self.assertEquals(serializer.data['first_name'], user.first_name)
        self.assertEquals(serializer.data['last_name'], user.last_name)

    def test_serializer_does_not_accept_email_that_already_exists(self):
        data = {'username': 'dummy', 'email': 'dummy@mail.net', 'password': 'secret'}
        serializer = APIUserSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        data = {'username': 'dummy2', 'email': 'dummy@mail.net', 'password': 'cantstopthis'}
        serializer = APIUserSerializer(data=data, context={'request': APIRequestFactory().get('/')})

        self.assertFalse(serializer.is_valid())
        self.assertEquals(set(serializer.errors), {'email'})


class LocationSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        data = {'location_name': 'somewhere', 'address': 'anyplace', 'map_url': 'http://www.place.com'}
        serializer = LocationSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        self.assertCountEqual(serializer.data.keys(),
                              ['location_name', 'address', 'map_url'])

    def test_serializer_can_create_location_object(self):
        data = {'location_name': 'somewhere', 'address': 'anyplace', 'map_url': 'http://www.place.com'}
        serializer = LocationSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(Location.objects.count(), 1)

    def test_serializer_can_serialize_location_object_properly(self):
        data = {'location_name': 'somewhere', 'address': 'anyplace', 'map_url': 'http://www.place.com'}
        serializer = LocationSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        location = Location.objects.first()

        self.assertEquals(serializer.data['location_name'], location.location_name)
        self.assertEquals(serializer.data['address'], location.address)
        self.assertEquals(serializer.data['map_url'], location.map_url)


class EventInfoSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        data = {'name': 'dummyevent', 'slug': 'dummy-event', 'cover_image': 'http://image.com',
                'short_description': 'short', 'description': 'desc',
                'start_at': '2019-01-01T12:0:0', 'end_at': '2019-01-02T12:0:0', 'price': '1000',
                'locations': [
                    {'location_name': 'place1', 'address': 'anywhere1', 'map_url': 'http://place.com'},
                    {'location_name': 'place2', 'address': 'anywhere2', 'map_url': 'http://place.com'}
                ]}
        serializer = EventInfoSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertCountEqual(serializer.data.keys(),
                              ['url', 'id', 'name', 'slug', 'cover_image', 'short_description',
                               'description', 'start_at', 'end_at', 'price', 'locations'])

    def test_serializer_can_create_event_info_object(self):
        data = {'name': 'dummyevent', 'slug': 'dummy-event', 'cover_image': 'http://image.com',
                'short_description': 'short', 'description': 'desc',
                'start_at': '2019-01-01T12:0:0', 'end_at': '2019-01-02T12:0:0', 'price': '1000'}
        serializer = EventInfoSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(EventInfo.objects.count(), 1)

    def test_serializer_can_serialize_event_info_object_properly(self):
        data = {'name': 'dummyevent', 'slug': 'dummy-event', 'cover_image': 'http://image.com',
                'short_description': 'short', 'description': 'desc',
                'start_at': '2019-01-01T12:0:0', 'end_at': '2019-01-02T12:0:0', 'price': '1000'}
        serializer = EventInfoSerializer(data=data, context={'request': APIRequestFactory().get('/')})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        eventinfo = EventInfo.objects.first()

        self.assertEquals(serializer.data['name'], eventinfo.name)
        self.assertEquals(serializer.data['slug'], eventinfo.slug)
        self.assertEquals(serializer.data['cover_image'], eventinfo.cover_image)
        self.assertEquals(serializer.data['short_description'], eventinfo.short_description)
        self.assertEquals(serializer.data['description'], eventinfo.description)
        self.assertEquals(serializer.data['start_at'], timezone.localtime(eventinfo.start_at).isoformat())
        self.assertEquals(serializer.data['end_at'], timezone.localtime(eventinfo.end_at).isoformat())
        self.assertEquals(serializer.data['price'], str(eventinfo.price))


class EventSlotSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        data = {'event': eventinfo.id, 'slot': 10, 'payment_limit_hour': 10, 'is_active': True}
        serializer = EventSlotSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertCountEqual(serializer.data.keys(),
                              ['id', 'event', 'slot', 'payment_limit_hour',
                               'is_active', 'available_slots'])

    def test_serializer_can_create_event_slot_object(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        data = {'event': eventinfo.id, 'slot': 10, 'payment_limit_hour': 10, 'is_active': True}
        serializer = EventSlotSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(EventSlot.objects.count(), 1)

    def test_serializer_can_serialize_event_slot_object_properly(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        data = {'event': eventinfo.id, 'slot': 10, 'payment_limit_hour': 10, 'is_active': True}
        serializer = EventSlotSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        eventslot = EventSlot.objects.first()

        self.assertEquals(serializer.data['id'], eventslot.id)
        self.assertEquals(serializer.data['event'], eventslot.event.id)
        self.assertEquals(serializer.data['slot'], eventslot.slot)
        self.assertEquals(serializer.data['payment_limit_hour'], eventslot.payment_limit_hour)
        self.assertEquals(serializer.data['is_active'], eventslot.is_active)
        self.assertEquals(serializer.data['available_slots'], eventslot.slot)

    def test_serializer_available_slots_should_be_decreased_when_there_exists_a_complete_booking(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        UserEventBooking.objects.create(user_event=user_event, event_slot=slot,
                                        status=UserEventBooking.COMPLETE)
        serializer = EventSlotSerializer(instance=slot)

        self.assertEquals(serializer.data['available_slots'], 9)

    def test_serializer_available_slots_slot_be_decreased_when_there_exists_a_pending_booking(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        UserEventBooking.objects.create(user_event=user_event, event_slot=slot,
                                        status=UserEventBooking.PENDING)
        serializer = EventSlotSerializer(instance=slot)

        self.assertEquals(serializer.data['available_slots'], 9)

    def test_serializer_available_slots_should_be_decreased_when_there_exists_a_non_expired_incomplete_booking(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        attempted_booking_datetime = datetime(2019, 7, 18, 10, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=attempted_booking_datetime)):
            UserEventBooking.objects.create(user_event=user_event, event_slot=slot)
            serializer = EventSlotSerializer(instance=slot)

            self.assertEquals(serializer.data['available_slots'], 9)

    def test_serializer_available_slots_should_not_be_decreased_when_there_exists_an_expired_booking(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        attempted_booking_datetime = datetime(2019, 7, 18, 10, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=attempted_booking_datetime)):
            booking = UserEventBooking.objects.create(user_event=user_event, event_slot=slot)

        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=booking.booking_expired_at)):
            serializer = EventSlotSerializer(instance=slot)

            self.assertEquals(serializer.data['available_slots'], slot.slot)

    def test_serializer_available_slots_should_decrease_accordingly_to_mixed_booking_statuses(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        attempted_booking_datetime = datetime(2019, 7, 18, 10, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=attempted_booking_datetime)):
            UserEventBooking.objects.create(user_event=user_event, event_slot=slot)  # Incomplete, but not expired
            UserEventBooking.objects.create(user_event=user_event, event_slot=slot,
                                            status=UserEventBooking.COMPLETE)
            UserEventBooking.objects.create(user_event=user_event, event_slot=slot,
                                            status=UserEventBooking.PENDING)

            serializer = EventSlotSerializer(instance=slot)

            self.assertEquals(serializer.data['available_slots'], 7)


class UserEventSerializerTests(APITestCase):
    request_factory = APIRequestFactory().get('/')

    def test_serializer_contains_expected_fields(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        event_info = EventInfo.objects.create(name='dummyevent',
                                              short_description='shortdesc',
                                              description='desc',
                                              start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                              end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                              price=10000)
        user_event = UserEvent.objects.create(user=user, event=event_info)

        serializer = UserEventSerializer(instance=user_event, context={'request': self.request_factory})

        self.assertCountEqual(serializer.data.keys(),
                              ['id', 'user', 'event', 'bookings'])

    def test_serializer_can_create_user_event_object(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        data = {
            'user': user.id,
            'event': EventInfoSerializer(instance=eventinfo, context={'request': self.request_factory}).data
        }

        serializer = UserEventSerializer(data=data)
        serializer.is_valid()
        serializer.save()

        self.assertEquals(UserEvent.objects.count(), 1)

    def test_serializer_can_serialize_user_event_object_properly(self):
        user = APIUser.objects.create(username='dummy', password='secret')
        event_info = EventInfo.objects.create(name='dummyevent',
                                              short_description='shortdesc',
                                              description='desc',
                                              start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                              end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                              price=10000)
        event_slot = EventSlot.objects.create(event=event_info, slot=1, payment_limit_hour=1, is_active=True)
        user_event = UserEvent.objects.create(user=user, event=event_info)
        event_booking = UserEventBooking.objects.create(user_event=user_event, event_slot=event_slot)
        serializer = UserEventSerializer(instance=user_event, context={'request': self.request_factory})

        self.assertEquals(serializer.data['id'], user_event.id)
        self.assertEquals(serializer.data['user'], user_event.user.id)
        self.assertEquals(serializer.data['event'], EventInfoSerializer(instance=user_event.event,
                                                                        context={'request': self.request_factory}
                                                                        ).data)
        self.assertEquals(serializer.data['bookings'], UserEventBookingSerializer(instance=[event_booking],
                                                                                  many=True).data)


class UserEventBookingSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        mocked_datetime = datetime(2019, 7, 18, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            data = {'user_event': user_event.id, 'event_slot': slot.id}
            serializer = UserEventBookingSerializer(data=data)
            serializer.is_valid()
            serializer.save()

        self.assertCountEqual(serializer.data.keys(),
                              ['id', 'user_event', 'event_slot', 'created',
                               'booking_expired_at', 'status',
                               'payment_confirmation_image'])

    def test_serializer_can_create_user_event_booking_object(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        mocked_datetime = datetime(2019, 7, 18, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            data = {'user_event': user_event.id, 'event_slot': slot.id}
            serializer = UserEventBookingSerializer(data=data)
            serializer.is_valid()
            serializer.save()

        self.assertEquals(UserEventBooking.objects.count(), 1)

    def test_serializer_can_serializer_user_event_booking_object_properly(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        mocked_datetime = datetime(2019, 7, 18, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            data = {'user_event': user_event.id, 'event_slot': slot.id}
            serializer = UserEventBookingSerializer(data=data)
            serializer.is_valid()
            serializer.save()
        booking_object = UserEventBooking.objects.first()

        self.assertEquals(serializer.data['id'], booking_object.id)
        self.assertEquals(serializer.data['user_event'], booking_object.user_event.id)
        self.assertEquals(serializer.data['event_slot'], booking_object.event_slot.id)
        self.assertEquals(serializer.data['created'], timezone.localtime(booking_object.created).isoformat())
        self.assertEquals(serializer.data['booking_expired_at'],
                          timezone.localtime(booking_object.booking_expired_at).isoformat())
        self.assertEquals(serializer.data['status'], booking_object.status)
        self.assertEquals(serializer.data['payment_confirmation_image'], None)

    def test_serializer_should_validate_invalid_user_event(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        data = {'user_event': 1, 'event_slot': slot.id}
        serializer = UserEventBookingSerializer(data=data)

        self.assertFalse(serializer.is_valid())
        self.assertEquals(set(serializer.errors), {'user_event'})

    def test_serializer_should_validate_invalid_event_slot(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)

        data = {'user_event': user_event.id, 'event_slot': 1}
        serializer = UserEventBookingSerializer(data=data)

        self.assertFalse(serializer.is_valid())
        self.assertEquals(set(serializer.errors), {'event_slot'})

    def test_serializer_should_validate_mismatch_user_event_and_event_slot(self):
        eventinfo1 = EventInfo.objects.create(name='dummyevent',
                                              short_description='shortdesc',
                                              description='desc',
                                              start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                              end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                              price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo1)

        eventinfo2 = EventInfo.objects.create(name='dummyevent',
                                              short_description='shortdesc',
                                              description='desc',
                                              start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                              end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                              price=10000)
        slot = EventSlot.objects.create(event=eventinfo2, slot=10,
                                        payment_limit_hour=10, is_active=True)

        data = {'user_event': user_event.id, 'event_slot': slot.id}
        serializer = UserEventBookingSerializer(data=data)

        self.assertFalse(serializer.is_valid())

    def test_serializer_should_validate_late_attempt_of_booking(self):
        eventinfo = EventInfo.objects.create(name='dummyevent',
                                             short_description='shortdesc',
                                             description='desc',
                                             start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
                                             end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
                                             price=10000)
        user = APIUser.objects.create(username='dummy', password='secret')
        user_event = UserEvent.objects.create(user=user, event=eventinfo)
        slot = EventSlot.objects.create(event=eventinfo, slot=10,
                                        payment_limit_hour=10, is_active=True)

        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=eventinfo.start_at)):
            data = {'user_event': user_event.id, 'event_slot': slot.id}
            serializer = UserEventBookingSerializer(data=data)

            self.assertFalse(serializer.is_valid())


class BlogArticleSerializerTests(APITestCase):

    def setUp(self):
        self.author = APIUser.objects.create(username='author', password='secret')
        self.tag = BlogTag.objects.create(name='tag 1')
        self.category = BlogCategory.objects.create(name='category 1', description='desc')

    def test_serializer_contains_expected_fields(self):
        article = BlogArticle.objects.create(
            title='title',
            headline='headline',
            content='content',
            author=self.author
        )
        article.tags.add(self.tag)
        article.categories.add(self.category)
        serializer = BlogArticleSerializer(
            instance=article,
            context={'request': APIRequestFactory().get('/')}
        )

        self.assertCountEqual(
            serializer.data.keys(),
            ['url', 'created', 'title', 'headline', 'cover_image',
             'content', 'slug', 'author', 'tags', 'categories']
        )
        self.assertCountEqual(
            serializer.data['author'].keys(),
            ['name', 'profile_picture', 'about']
        )
        self.assertCountEqual(
            serializer.data['tags'][0].keys(),
            ['name', 'slug']
        )
        self.assertCountEqual(
            serializer.data['categories'][0].keys(),
            ['name', 'description', 'slug']
        )

    def test_serializer_can_create_blog_article_object(self):
        data = {
            'title': 'title',
            'headline': 'headline',
            'content': 'content',
            'author': {
                'username': 'Author2',
                'email': 'author2@mail.net',
                'password': 'secret'
            }
        }
        serializer = BlogArticleSerializer(
            data=data,
            context={'request': APIRequestFactory().get('/')}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(BlogArticle.objects.count(), 1)

    def test_serializer_can_serialize_blog_article_object_properly(self):
        article = BlogArticle.objects.create(
            title='title',
            headline='headline',
            content='content',
            author=self.author
        )
        article.tags.add(self.tag)
        article.categories.add(self.category)
        serializer = BlogArticleSerializer(
            instance=article,
            context={'request': APIRequestFactory().get('/')}
        )
        data = serializer.data

        self.assertEquals(data['created'], timezone.localtime(article.created).isoformat())
        self.assertEquals(data['title'], article.title)
        self.assertEquals(data['headline'], article.headline)
        self.assertEquals(data['content'], article.content)
        self.assertEquals(data['author']['name'], article.author.name)
        self.assertEquals(data['author']['profile_picture'], article.author.profile_picture)
        self.assertEquals(data['author']['about'], article.author.about)
        self.assertEquals(data['tags'][0]['name'], article.tags.first().name)
        self.assertEquals(data['tags'][0]['slug'], article.tags.first().slug)
        self.assertEquals(data['categories'][0]['name'], article.categories.first().name)
        self.assertEquals(data['categories'][0]['description'], article.categories.first().description)
        self.assertEquals(data['categories'][0]['slug'], article.categories.first().slug)


class BlogTagSerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        tag = BlogTag.objects.create(name='tag 1')
        serializer = BlogTagSerializer(instance=tag)

        self.assertCountEqual(
            serializer.data.keys(),
            ['name', 'slug']
        )

    def test_serializer_can_create_blog_tag_object(self):
        data = {
            'name': 'tag 1'
        }
        serializer = BlogTagSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(BlogTag.objects.count(), 1)

    def test_serializer_can_serialize_blog_tag_object_properly(self):
        tag = BlogTag.objects.create(name='tag 1')
        serializer = BlogTagSerializer(instance=tag)
        data = serializer.data

        self.assertEquals(data['name'], tag.name)
        self.assertEquals(data['slug'], tag.slug)


class BlogCategorySerializerTests(APITestCase):

    def test_serializer_contains_expected_fields(self):
        category = BlogCategory.objects.create(name='category 1', description='desc')
        serializer = BlogCategorySerializer(instance=category)

        self.assertCountEqual(
            serializer.data.keys(),
            ['name', 'description', 'slug']
        )

    def test_serializer_can_create_blog_category_object(self):
        data = {
            'name': 'category 1',
            'description': 'desc'
        }
        serializer = BlogCategorySerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertEquals(BlogCategory.objects.count(), 1)

    def test_serializer_can_serialize_blog_category_object_properly(self):
        category = BlogCategory.objects.create(name='category 1', description='desc')
        serializer = BlogCategorySerializer(instance=category)
        data = serializer.data

        self.assertEquals(data['name'], category.name)
        self.assertEquals(data['description'], category.description)
        self.assertEquals(data['slug'], category.slug)
