import io
import json
from datetime import datetime
from unittest import mock

import pytz
from PIL import Image
from django.core import mail
from django.core.files import File
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory

from api.models import APIUser, EventInfo, UserEvent, EventSlot, UserEventBooking, BlogCategory, BlogArticle, BlogTag
from api.serializers import UserEventSerializer, EventInfoSerializer, UserEventBookingSerializer, APIUserSerializer, \
    EventSlotSerializer, BlogArticleSerializer
from api.signals import booking_created, booking_payment_uploaded
from api.tests.utils import CatchSignal
from api.views import MakeEventBookingView


class UsernameAvailabilityEndpointTests(APITestCase):

    def test_response_should_404_when_available_username_given_to_request(self):
        url = reverse('username-validation', kwargs={'username': 'dummy'})
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_response_should_200_when_not_available_username_given_to_request(self):
        user = APIUser.objects.create(username='dummy', email='dummy@mail.com', password='secret')
        url = reverse('username-validation', kwargs={'username': user.username})
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class EmailAvailabilityEndpointTests(APITestCase):

    def test_response_should_404_when_available_email_given_to_request(self):
        url = reverse('email-validation', kwargs={'email': 'dummy@mail.com'})
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_response_should_200_when_not_available_email_given_to_request(self):
        user = APIUser.objects.create(username='dummy', email='dummy@mail.com', password='secret')
        url = reverse('email-validation', kwargs={'email': user.email})
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class UserInfoViewTests(APITestCase):

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.user = APIUser.objects.first()

    def test_GET_request_should_return_serialized_user_info(self):
        url = reverse('user-info')
        serializer = APIUserSerializer(
            instance=self.user,
            context={'request': APIRequestFactory().get('/')}
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['user_id'], serializer.data['user_id'])
        self.assertEquals(response.data['name'], serializer.data['name'])
        self.assertEquals(response.data['email'], serializer.data['email'])
        self.assertEquals(response.data['phone_number'], serializer.data['phone_number'])
        self.assertEquals(response.data['birth_date'], serializer.data['birth_date'])
        self.assertEquals(response.data['line_id'], serializer.data['line_id'])
        self.assertEquals(response.data['location'], serializer.data['location'])
        self.assertEquals(response.data['education'], serializer.data['education'])
        self.assertEquals(response.data['institution'], serializer.data['institution'])
        self.assertEquals(response.data['is_email_verified'], serializer.data['is_email_verified'])
        self.assertEquals(response.data['is_signed_with_social_account'],
                          serializer.data['is_signed_with_social_account'])
        self.assertEquals(response.data['is_profile_complete'], serializer.data['is_profile_complete'])


class EventInfoDetailViewTests(APITestCase):

    def test_GET_request_with_valid_slug_should_return_serialized_event_object(self):
        event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        url = reverse(
            viewname='event-detail',
            kwargs={'slug': event.slug}
        )
        response = self.client.get(path=url)
        serializer = EventInfoSerializer(
            instance=event,
            context={'request': APIRequestFactory().get('/')}
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, serializer.data)

    def test_GET_request_with_invalid_slug_should_404(self):
        url = reverse(
            viewname='event-detail',
            kwargs={'slug': 'nonexistent-slug'}
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)


class SlotsOfAnEventViewTests(APITestCase):

    def setUp(self):
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=False
        )
        EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=False
        )

    def test_GET_request_with_unavailable_event_should_404(self):
        url = reverse(
            viewname='event-slots',
            kwargs={'event_id': 10}
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_GET_request_without_query_should_return_all_serialized_slot_objects(self):
        url = reverse(
            viewname='event-slots',
            kwargs={'event_id': self.event.id}
        )
        response = self.client.get(path=url)
        serializer = EventSlotSerializer(
            instance=EventSlot.objects.all(),
            many=True
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['slots'], serializer.data)

    def test_GET_request_with_True_active_query_parameter_should_return_the_only_active_serialized_slot_object(self):
        url = "{}?active=True".format(
            reverse(
                viewname='event-slots',
                kwargs={'event_id': self.event.id}
            )
        )
        response = self.client.get(path=url)
        serializer = EventSlotSerializer(
            instance=EventSlot.objects.filter(is_active=True),
            many=True
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['slots'], serializer.data)

    def test_GET_request_with_False_active_query_parameter_should_return_inactive_serialized_slot_objects(self):
        url = "{}?active=False".format(
            reverse(
                viewname='event-slots',
                kwargs={'event_id': self.event.id}
            )
        )
        response = self.client.get(path=url)
        serializer = EventSlotSerializer(
            instance=EventSlot.objects.filter(is_active=False),
            many=True
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['slots'], serializer.data)

    def test_GET_request_with_invalid_active_query_parameter_should_400(self):
        url = "{}?active=Invalid".format(
            reverse(
                viewname='event-slots',
                kwargs={'event_id': self.event.id}
            )
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserEventViewTests(APITestCase):
    url = reverse('my-events')

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )

    def test_GET_request_should_return_serialized_user_event_objects_from_requesting_user(self):
        UserEvent.objects.create(
            user=APIUser.objects.first(),
            event=self.event
        )
        user_events = UserEvent.objects.filter(user=APIUser.objects.first())
        serializer = UserEventSerializer(
            instance=user_events,
            many=True,
            context={'request': APIRequestFactory().get('/')}
        )
        response = self.client.get(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['user_events'], serializer.data)

    def test_POST_valid_request_should_create_user_event_object(self):
        valid_data = {
            'user': APIUser.objects.first().id,
            'event': EventInfoSerializer(
                instance=self.event,
                context={'request': APIRequestFactory().get('/')}
            ).data
        }
        response = self.client.post(
            path=self.url,
            data=json.dumps(valid_data),
            content_type='application/json'
        )

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(UserEvent.objects.count(), 1)

    def test_POST_invalid_request_should_400(self):
        invalid_data = {
            'user': '',
            'event': ''
        }
        response = self.client.post(
            path=self.url,
            data=json.dumps(invalid_data),
            content_type='application/json'
        )

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserEventBookingViewTests(APITestCase):

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        self.user_event = UserEvent.objects.create(
            user=APIUser.objects.first(),
            event=self.event
        )
        self.event_slot = EventSlot.objects.create(
            event=self.event, slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        self.url = reverse(
            viewname='my-event-bookings',
            kwargs={'user_event_id': self.user_event.id}
        )

    def test_GET_request_should_return_serialized_booking_object_from_requesting_user(self):
        UserEventBooking.objects.create(
            user_event=self.user_event,
            event_slot=self.event_slot
        )

        response = self.client.get(path=self.url)
        bookings = UserEventBooking.objects.filter(user_event=self.user_event)
        serializer = UserEventBookingSerializer(
            instance=bookings,
            many=True
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, serializer.data)

    def test_GET_request_with_non_existing_user_event_should_404(self):
        url = reverse(
            viewname='my-event-bookings',
            kwargs={'user_event_id': 10}
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_POST_valid_request_should_create_user_event_booking_object(self):
        with mock.patch(
                'django.utils.timezone.now',
                mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
            valid_data = {
                'user_event': self.user_event.id,
                'event_slot': self.event_slot.id
            }
            response = self.client.post(
                path=self.url,
                data=json.dumps(valid_data),
                content_type='application/json'
            )

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(UserEventBooking.objects.count(), 1)

    def test_POST_invalid_request_should_400(self):
        invalid_data = {
            'user_event': '',
            'event_slot': ''
        }
        response = self.client.post(
            path=self.url,
            data=json.dumps(invalid_data),
            content_type='application/json'
        )

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)


class EventBookingPaymentConfirmationViewTests(APITestCase):

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.user = APIUser.objects.first()
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        self.user_event = UserEvent.objects.create(
            user=APIUser.objects.first(),
            event=self.event
        )
        self.event_slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=1,
            is_active=True
        )
        self.event_booking = UserEventBooking.objects.create(
            user_event=self.user_event,
            event_slot=self.event_slot
        )
        self.url = reverse(
            viewname='payment-confirmation',
            kwargs={
                'user_event_id': self.user_event.id,
                'booking_id': self.event_booking.id
            }
        )
        mail.outbox = []

    def test_PATCH_request_accepts_image_file(self):
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        data = {
            'payment_confirmation_image': file
        }
        with mock.patch(
                'django.utils.timezone.now',
                mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
            response = self.client.patch(
                path=self.url,
                data=data,
                format='multipart'
            )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['message'], 'Payment image successfully uploaded.')

    def test_PATCH_does_not_accepts_non_image_file(self):
        file = mock.MagicMock(spec=File)
        file.name = 'test.txt'

        data = {
            'payment_confirmation_image': file
        }
        with mock.patch(
                'django.utils.timezone.now',
                mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
            response = self.client.patch(
                path=self.url,
                data=data,
                format='multipart'
            )

        self.assertEquals(response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def test_successful_upload_should_send_signal(self):
        with CatchSignal(booking_payment_uploaded) as handler:
            file = io.BytesIO()
            image = Image.new('RGBA', size=(100, 100))
            image.save(file, 'png')
            file.name = 'test.png'
            file.seek(0)

            data = {
                'payment_confirmation_image': file
            }
            with mock.patch(
                    'django.utils.timezone.now',
                    mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
                self.client.patch(
                    path=self.url,
                    data=data,
                    format='multipart'
                )

        handler.assert_called_once()

    def test_unsuccessful_upload_should_not_send_signal(self):
        with CatchSignal(booking_payment_uploaded) as handler:
            file = mock.MagicMock(spec=File)
            file.name = 'test.txt'

            data = {
                'payment_confirmation_image': file
            }
            with mock.patch(
                    'django.utils.timezone.now',
                    mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
                self.client.patch(
                    path=self.url,
                    data=data,
                    format='multipart'
                )

        handler.assert_not_called()

    def test_successful_upload_should_send_email_to_the_user(self):
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        data = {
            'payment_confirmation_image': file
        }
        with mock.patch(
                'django.utils.timezone.now',
                mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
            self.client.patch(
                path=self.url,
                data=data,
                format='multipart'
            )

        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to[0], self.user.email)

    def test_unsuccessful_upload_should_not_send_email_to_the_user(self):
        file = mock.MagicMock(spec=File)
        file.name = 'test.txt'

        data = {
            'payment_confirmation_image': file
        }
        with mock.patch(
                'django.utils.timezone.now',
                mock.Mock(return_value=datetime(2000, 1, 1, tzinfo=pytz.utc))):
            self.client.patch(
                path=self.url,
                data=data,
                format='multipart'
            )

        self.assertEquals(len(mail.outbox), 0)


class MakeEventBookingViewTests(APITestCase):

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.user = APIUser.objects.first()
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        self.event_slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=6,
            is_active=True
        )
        self.url = reverse(
            viewname='make-event-booking',
            kwargs={'event_id': self.event.id}
        )
        mail.outbox = []

    def test_successful_initial_booking_should_create_user_event_and_booking_object(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(UserEvent.objects.count(), 1)
        self.assertEquals(UserEventBooking.objects.count(), 1)

    def test_successful_booking_attempt_with_previous_expired_booking_should_create_booking_object(self):
        user_event = UserEvent.objects.create(user=self.user, event=self.event)
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 17, tzinfo=pytz.utc))):
            UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=self.event_slot
            )
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(UserEventBooking.objects.count(), 2)

    def test_late_booking_attempt_should_be_unauthorized(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 20, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_booking_attempt_with_existing_not_expired_previous_booking_should_be_unauthorized(self):
        user_event = UserEvent.objects.create(user=self.user, event=self.event)
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 17, tzinfo=pytz.utc))):
            UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=self.event_slot
            )
            response = self.client.post(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_late_booking_attempt_with_existing_not_expired_previous_booking_should_be_unauthorized(self):
        user_event = UserEvent.objects.create(user=self.user, event=self.event)
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 17, tzinfo=pytz.utc))):
            UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=self.event_slot
            )
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 20, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_booking_attempt_on_full_slot_should_be_unauthorized(self):
        # First booking from one user
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            self.client.post(path=self.url)

        # Creates another user to attempt another booking
        data = {
            'username': 'dummy_username2',
            'email': 'dummyemail@mail2.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)

        # Second attempt booking
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_booking_attempt_after_previous_booking_is_complete_should_be_unauthorized(self):
        user_event = UserEvent.objects.create(user=self.user, event=self.event)
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 17, tzinfo=pytz.utc))):
            UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=self.event_slot,
                status=UserEventBooking.COMPLETE
            )

        # Second attempt booking
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            response = self.client.post(path=self.url)
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_successful_booking_should_send_signal(self):
        with CatchSignal(booking_created) as handler:
            with mock.patch('django.utils.timezone.now',
                            mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
                self.client.post(path=self.url)

        handler.assert_called_once_with(
            signal=booking_created,
            sender=MakeEventBookingView,
            user=self.user,
            booking=self.user.userevent_set.first().usereventbooking_set.first(),
        )

    def test_unsuccessful_booking_should_not_send_signal(self):
        with CatchSignal(booking_created) as handler:
            with mock.patch('django.utils.timezone.now',
                            mock.Mock(return_value=datetime(2019, 7, 20, tzinfo=pytz.utc))):
                self.client.post(path=self.url)

        handler.assert_not_called()

    def test_successful_booking_should_send_email_to_the_user(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            self.client.post(path=self.url)

        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to[0], self.user.email)

    def test_unsuccessful_booking_should_not_send_email_to_the_user(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 20, tzinfo=pytz.utc))):
            self.client.post(path=self.url)

        self.assertEquals(len(mail.outbox), 0)


class SpecificBookingViewTests(APITestCase):

    def setUp(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(
            path=reverse('register-account'),
            data=data
        )
        user_token = registration_response.data.get('token')
        self.user = APIUser.objects.first()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)
        self.event = EventInfo.objects.create(
            name='dummyevent',
            short_description='shortdesc',
            description='desc',
            start_at=datetime(2019, 7, 19, 10, tzinfo=pytz.utc),
            end_at=datetime(2019, 7, 19, 16, tzinfo=pytz.utc),
            price=10000
        )
        self.event_slot = EventSlot.objects.create(
            event=self.event,
            slot=1,
            payment_limit_hour=6,
            is_active=True
        )
        self.user_event = UserEvent.objects.create(
            user=self.user,
            event=self.event
        )

    def test_GET_request_should_return_serialized_booking_object(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            booking = UserEventBooking.objects.create(
                user_event=self.user_event,
                event_slot=self.event_slot
            )
        url = reverse(
            viewname='specific-booking',
            kwargs={
                'user_event_id': self.user_event.id,
                'booking_id': booking.id
            }
        )
        serializer = UserEventBookingSerializer(instance=booking)
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, serializer.data)

    def test_GET_request_with_not_existing_user_event_should_404(self):
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=datetime(2019, 7, 18, tzinfo=pytz.utc))):
            booking = UserEventBooking.objects.create(
                user_event=self.user_event,
                event_slot=self.event_slot
            )
        url = reverse(
            viewname='specific-booking',
            kwargs={
                'user_event_id': 10,
                'booking_id': booking.id
            }
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_GET_request_with_not_existing_booking_should_404(self):
        url = reverse(
            viewname='specific-booking',
            kwargs={
                'user_event_id': self.user_event.id,
                'booking_id': 10
            }
        )
        response = self.client.get(path=url)

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)


class BlogArticlesFromCategoryViewTests(APITestCase):

    def setUp(self):
        self.author = APIUser.objects.create(
            username='test',
            email='email@mail.net',
            password='secret'
        )
        self.category = BlogCategory.objects.create(name='category 1', description='desc')

    def test_GET_request_should_return_serialized_article_response(self):
        article = BlogArticle.objects.create(
            title='Title 1',
            headline='Headline 1',
            content='Content 1',
            author=self.author,
        )
        article.categories.add(self.category)

        url = reverse('blog-categories-articles-from-category', kwargs={'slug': self.category.slug})
        response = self.client.get(path=url)
        serialized_article = BlogArticleSerializer(
            instance=article,
            context={'request': APIRequestFactory().get('/')}
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['results'][0], serialized_article.data)
        self.assertEquals(response.data['results'][0]['categories'][0]['name'], self.category.name)
        self.assertEquals(response.data['results'][0]['categories'][0]['description'], self.category.description)
        self.assertEquals(response.data['results'][0]['categories'][0]['slug'], self.category.slug)


class BlogArticlesFromTagView(APITestCase):

    def setUp(self):
        self.author = APIUser.objects.create(
            username='test',
            email='email@mail.net',
            password='secret'
        )
        self.tag = BlogTag.objects.create(name='tag 1',)

    def test_GET_request_should_return_serialized_article_response(self):
        article = BlogArticle.objects.create(
            title='Title 1',
            headline='Headline 1',
            content='Content 1',
            author=self.author,
        )
        article.tags.add(self.tag)

        url = reverse('blog-tags-articles-from-tag', kwargs={'slug': self.tag.slug})
        response = self.client.get(path=url)
        serialized_article = BlogArticleSerializer(
            instance=article,
            context={'request': APIRequestFactory().get('/')}
        )

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['results'][0], serialized_article.data)
        self.assertEquals(response.data['results'][0]['tags'][0]['name'], self.tag.name)
        self.assertEquals(response.data['results'][0]['tags'][0]['slug'], self.tag.slug)
