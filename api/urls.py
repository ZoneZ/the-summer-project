from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import (
    EventBookingPaymentConfirmationView, EventInfoDetailView,
    EventInfoListView, MakeEventBookingView, SlotsOfAnEventView,
    SpecificBookingView, UserEventBookingView,
    UserEventView, UserViewSet, api_root,
    UsernameValidationView, EmailValidationView, UserInfoView, BlogArticleViewSet, BlogCategoryViewSet, BlogTagViewSet)

urlpatterns = [
    path('', api_root, name='api-root'),

    path('users/username-validation/<str:username>/', UsernameValidationView.as_view(), name='username-validation'),
    path('users/email-validation/<str:email>/', EmailValidationView.as_view(), name='email-validation'),

    path('me/', UserInfoView.as_view(), name='user-info'),
    path('me/events/', UserEventView.as_view(), name='my-events'),
    path('me/events/<int:event_id>/booking-creation/', MakeEventBookingView.as_view(), name='make-event-booking'),
    path('me/events/<int:user_event_id>/bookings/', UserEventBookingView.as_view(), name='my-event-bookings'),
    path('me/events/<int:user_event_id>/bookings/<int:booking_id>/',
         SpecificBookingView.as_view(), name='specific-booking'),
    path('me/events/<int:user_event_id>/bookings/<int:booking_id>/payment-proof/',
         EventBookingPaymentConfirmationView.as_view(), name='payment-confirmation'),

    path('events/', EventInfoListView.as_view(), name='events'),
    path('events/<str:slug>/', EventInfoDetailView.as_view(), name='event-detail'),
    path('events/<int:event_id>/slots/', SlotsOfAnEventView.as_view(), name='event-slots')
]

router = DefaultRouter()
router.register(
    prefix='users',
    viewset=UserViewSet,
    basename='users'
)
router.register(
    prefix='blog/articles',
    viewset=BlogArticleViewSet,
    basename='blog-articles'
)
router.register(
    prefix='blog/categories',
    viewset=BlogCategoryViewSet,
    basename='blog-categories'
)
router.register(
    prefix='blog/tags',
    viewset=BlogTagViewSet,
    basename='blog-tags'
)

urlpatterns += router.urls
