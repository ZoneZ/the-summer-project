from django.core.mail import EmailMultiAlternatives


class EmailBuilder:

    def __init__(self):
        self._email = EmailMultiAlternatives()

    def with_subject(self, subject):
        self._email.subject = subject
        return self

    def with_body(self, body):
        self._email.body = body
        return self

    def with_from_address(self, from_address):
        self._email.from_email = from_address
        return self

    def with_to_addresses(self, to_addresses):
        self._email.to = to_addresses
        return self

    def with_bcc(self, bcc):
        self._email.bcc = bcc
        return self

    def with_connection(self, connection):
        self._email.connection = connection
        return self

    def with_attachments(self, attachments):
        self._email.attachments = attachments
        return self

    def with_headers(self, headers):
        self._email.extra_headers = headers
        return self

    def with_cc(self, cc):
        self._email.cc = cc
        return self

    def with_reply_to_addresses(self, reply_to_addresses):
        self._email.reply_to = reply_to_addresses
        return self

    def with_alternative(self, content, mimetype):
        self._email.attach_alternative(
            content=content,
            mimetype=mimetype
        )
        return self

    def send(self):
        self._email.send()
