from django.utils import timezone
from rest_framework import generics, status, viewsets
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import (IsAdminUser, IsAuthenticated,
                                        IsAuthenticatedOrReadOnly)
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from api.models import (APIUser, EventInfo, EventSlot, UserEvent,
                        UserEventBooking, BlogArticle, BlogCategory, BlogTag)
from api.permissions import IsEmailVerified, IsOwner, IsOwnerOrAdmin
from api.serializers import (APIUserSerializer, EventInfoSerializer,
                             EventSlotSerializer, UserEventBookingSerializer,
                             UserEventSerializer, BlogArticleSerializer, BlogCategorySerializer, BlogTagSerializer)
from api.signals import booking_created, booking_payment_uploaded
from api.pagination import BlogResponsePagination


@api_view(['GET'])
@permission_classes((IsAuthenticatedOrReadOnly,))
def api_root(request, format=None):
    return Response({
        'events': reverse('events', request=request, format=format),
        'blog': reverse('blog-articles-list', request=request, format=format)
    })


class UserViewSet(viewsets.ModelViewSet):
    queryset = APIUser.objects.all()
    serializer_class = APIUserSerializer
    http_method_names = ['get', 'put', 'patch', 'head', 'options', 'trace']

    def get_permissions(self):
        if self.action == "list":
            permission_types = [IsAuthenticated, IsAdminUser, IsEmailVerified]
        elif self.action == "retrieve":
            permission_types = [IsAuthenticated, IsOwnerOrAdmin]
        elif self.action == "update" or self.action == "partial_update":
            permission_types = [IsAuthenticated, IsOwner]
        else:
            permission_types = []

        return [permission() for permission in permission_types]


class UserInfoView(APIView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        serializer = APIUserSerializer(
            instance=user,
            context={'request': self.request}
        )
        return Response(serializer.data)


class EventInfoListView(generics.ListAPIView):
    queryset = EventInfo.objects.all()
    serializer_class = EventInfoSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)


class EventInfoDetailView(generics.RetrieveAPIView):
    queryset = EventInfo.objects.all()
    serializer_class = EventInfoSerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticatedOrReadOnly,)


class SlotsOfAnEventView(APIView):
    accepted_query_params = ['True', 'False']
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        event = get_object_or_404(queryset=EventInfo.objects.all(), id=self.kwargs['event_id'])
        if 'active' in self.request.query_params.keys():
            query = self.request.query_params.get('active')
            if query in self.accepted_query_params:
                event_slots = EventSlot.objects.filter(event=event, is_active=query)
            else:
                return Response({'Error': 'Value of "active" parameter must be either "True" or "False".'},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            event_slots = EventSlot.objects.filter(event=event)
        return Response({'slots': EventSlotSerializer(event_slots, many=True).data})


class UserEventView(APIView):

    def get(self, request, *args, **kwargs):
        user_event = UserEvent.objects.filter(user=self.request.user)
        serialized_user_event = UserEventSerializer(instance=user_event,
                                                    many=True,
                                                    context={'request': self.request})
        return Response({'user_events': serialized_user_event.data})

    def post(self, request, *args, **kwargs):
        serializer = UserEventSerializer(data=self.request.data, context={'request': self.request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserEventBookingView(APIView):

    def get(self, request, *args, **kwargs):
        user_event = get_object_or_404(queryset=UserEvent.objects.all(),
                                       user_id=self.request.user.id,
                                       id=self.kwargs.get('user_event_id'))
        event_booking = UserEventBooking.objects.filter(user_event=user_event).order_by('-created')
        serialized_event_booking = UserEventBookingSerializer(instance=event_booking, many=True)
        return Response(serialized_event_booking.data)

    def post(self, request, *args, **kwargs):
        serializer = UserEventBookingSerializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SpecificBookingView(APIView):

    def get(self, request, *args, **kwargs):
        user_event = get_object_or_404(queryset=UserEvent.objects.all(),
                                       user_id=self.request.user.id,
                                       id=self.kwargs.get('user_event_id'))
        event_booking = get_object_or_404(queryset=UserEventBooking.objects.all(),
                                          user_event=user_event, id=self.kwargs.get('booking_id'))
        return Response(UserEventBookingSerializer(instance=event_booking).data)


class EventBookingPaymentConfirmationView(APIView):

    def patch(self, request, *args, **kwargs):
        user_event = get_object_or_404(queryset=UserEvent.objects.all(),
                                       user_id=self.request.user.id,
                                       id=self.kwargs.get('user_event_id'))
        event_booking = get_object_or_404(queryset=UserEventBooking.objects.all(),
                                          user_event=user_event, id=self.kwargs.get('booking_id'))
        self.request.data['user_event'] = event_booking.user_event.id
        self.request.data['event_slot'] = event_booking.event_slot.id
        serializer = UserEventBookingSerializer(instance=event_booking, data=self.request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            booking_payment_uploaded.send_robust(
                sender=self.__class__,
                user=self.request.user,
                booking=event_booking
            )
            return Response({'message': 'Payment image successfully uploaded.'})
        else:
            return Response(serializer.errors, status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)


class MakeEventBookingView(APIView):
    event = None
    previous_event_booking = None

    def post(self, request, *args, **kwargs):
        event_id = self.kwargs.get('event_id')
        self.event = get_object_or_404(queryset=EventInfo.objects.all(), id=event_id)
        user_event, created = UserEvent.objects.get_or_create(event=self.event, user_id=self.request.user.id)
        active_event_slot = get_object_or_404(queryset=EventSlot.objects.all(),
                                              event=self.event,
                                              is_active=True)

        self.previous_event_booking = UserEventBooking.objects.filter(user_event=user_event,
                                                                      event_slot=active_event_slot) \
            .order_by('-created') \
            .first()
        if self.can_create_booking() and active_event_slot.available_slots:
            created_booking = UserEventBooking.objects.create(
                user_event=user_event,
                event_slot=active_event_slot
            )
            booking_created.send_robust(
                sender=self.__class__,
                user=self.request.user,
                booking=created_booking,
            )
            return Response(
                UserEventBookingSerializer(instance=created_booking).data,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response({'Unauthorized': 'Booking cannot be created.'}, status=status.HTTP_401_UNAUTHORIZED)

    def can_create_booking(self):
        return self._has_expired_previous_booking() and \
               not self._attempt_after_booking_deadline()

    def _has_expired_previous_booking(self):
        if self.previous_event_booking and \
                self.previous_event_booking.status == UserEventBooking.NOT_COMPLETE:
            return self.previous_event_booking.is_payment_expired
        return True

    def _attempt_after_booking_deadline(self):
        return timezone.localtime() >= timezone.localtime(self.event.booking_deadline)


class UsernameValidationView(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        username = self.kwargs.get('username')
        if APIUser.objects.filter(username=username).exists():
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class EmailValidationView(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        email = self.kwargs.get('email')
        if APIUser.objects.filter(email=email).exists():
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class BlogResponseView(viewsets.ModelViewSet):
    permission_classes = ()
    pagination_class = BlogResponsePagination
    http_method_names = ['get', 'head', 'options', 'trace']


class BlogArticleViewSet(BlogResponseView):
    lookup_field = 'slug'
    queryset = BlogArticle.objects.all()
    serializer_class = BlogArticleSerializer


class BlogCategoryViewSet(BlogResponseView):
    lookup_field = 'slug'
    queryset = BlogCategory.objects.all()
    serializer_class = BlogCategorySerializer

    @action(methods=['get'], detail=True, url_name='articles-from-category')
    def articles(self, request, slug=None):
        articles = BlogArticle.objects.filter(categories__slug=slug).order_by('-created')
        paginated_articles = self.paginate_queryset(articles)
        serialized_articles = BlogArticleSerializer(
            instance=paginated_articles,
            many=True,
            context={'request': request}
        )
        return self.get_paginated_response(serialized_articles.data)


class BlogTagViewSet(BlogResponseView):
    lookup_field = 'slug'
    queryset = BlogTag.objects.all()
    serializer_class = BlogTagSerializer

    @action(methods=['get'], detail=True, url_name='articles-from-tag')
    def articles(self, request, slug=None):
        articles = BlogArticle.objects.filter(tags__slug=slug).order_by('-created')
        paginated_articles = self.paginate_queryset(articles)
        serialized_articles = BlogArticleSerializer(
            instance=paginated_articles,
            many=True,
            context={'request': request}
        )
        return self.get_paginated_response(serialized_articles.data)
