from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings


class UserRegistrationAdapter(DefaultAccountAdapter):

    def get_email_confirmation_url(self, request, emailconfirmation):
        url = settings.REGISTRATION_EMAIL_VERIFICATION_URL + str(emailconfirmation.key)
        return url
