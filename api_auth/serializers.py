from rest_auth.serializers import UserDetailsSerializer
from rest_framework import serializers


class KnoxSerializer(serializers.Serializer):
    token = serializers.CharField()
    user = UserDetailsSerializer()
