from allauth.account.models import EmailAddress
from allauth.account.signals import email_confirmed
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django_rest_passwordreset.signals import reset_password_token_created

from api.models import APIUser


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    context = {
        'current_user': reset_password_token.user,
        'username': reset_password_token.user.username,
        'email': reset_password_token.user.email,
        'reset_password_url': "{}?token={}".format(settings.USER_RESET_PASSWORD_URL,
                                                   reset_password_token.key)
    }

    email_html_message = render_to_string('email/user_reset_password.html', context=context)
    email_plaintext_message = render_to_string('email/user_reset_password.txt', context=context)

    message = EmailMultiAlternatives(
        subject="Password Reset for {}".format("Mentoria Indonesia Web"),
        body=email_plaintext_message,
        from_email="NoReply@mentoria.id",
        to=[reset_password_token.user.email]
    )
    message.attach_alternative(content=email_html_message, mimetype="text/html")
    message.send()


@receiver(email_confirmed)
def verify_email_confirmed(request, email_address, *args, **kwargs):
    user = APIUser.objects.get(email=email_address.email)
    user.is_email_verified = True
    user.save()


@receiver(post_save, sender=EmailAddress)
def verify_related_user_email(sender, instance, **kwargs):
    if instance.verified:
        user = APIUser.objects.get(email=instance.email)
        user.is_email_verified = True
        user.save()
