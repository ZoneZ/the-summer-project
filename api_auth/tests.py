from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount
from django.core import mail
from django.urls import reverse
from knox.models import AuthToken
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import APIUser


class RegistrationTests(APITestCase):

    register_url = reverse('register-account')

    def test_registration_with_valid_data_creates_user(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        response = self.client.post(path=self.register_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(APIUser.objects.count(), 1)

    def test_registered_user_has_email_address_object(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        self.client.post(path=self.register_url, data=data)
        new_user = APIUser.objects.get(username='dummy_username')

        self.assertEqual(EmailAddress.objects.count(), 1)
        self.assertEqual(EmailAddress.objects.first().user, new_user)
        self.assertEqual(EmailAddress.objects.first().email, new_user.email)

    def test_registered_user_has_token_object(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        self.client.post(path=self.register_url, data=data)
        new_user = APIUser.objects.get(username='dummy_username')

        self.assertEqual(AuthToken.objects.count(), 1)
        self.assertEqual(AuthToken.objects.first().user, new_user)

    def test_registration_with_invalid_email(self):
        data = {
            'username': 'dummy_username',
            'email': 'fakeemail',
            'password1': 'dummypassword',
            'password2': 'dummypassword',
        }
        response = self.client.post(path=self.register_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_with_mismatch_passwords(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypasswordzzz',
        }
        response = self.client.post(path=self.register_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_with_already_used_username(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypasswordzzz',
        }
        self.client.post(path=self.register_url, data=data)

        data['email'] = 'newemail@mail.net'
        response = self.client.post(path=self.register_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_with_already_used_email(self):
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypasswordzzz',
        }
        self.client.post(path=self.register_url, data=data)

        data['username'] = 'another_dummy_username'
        response = self.client.post(path=self.register_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class EmailVerificationTests(APITestCase):

    email_verification_url = reverse('send-email-verification')

    def setUp(self):
        url = reverse('register-account')
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(path=url, data=data)
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)

    def test_send_email_verification_to_unverified_email(self):
        response = self.client.post(path=self.email_verification_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'message': 'Email verification sent.'})
        self.assertGreaterEqual(len(mail.outbox), 1)

    def test_send_email_verification_to_already_verified_email(self):
        registered_user = APIUser.objects.get(username='dummy_username')
        email_address_object = EmailAddress.objects.get(user=registered_user)
        email_address_object.verified = True
        email_address_object.save()

        response = self.client.post(path=self.email_verification_url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'message': 'Email is already verified.'})


class PasswordChangeTests(APITestCase):

    password_change_url = reverse('password-change')

    def setUp(self):
        url = reverse('register-account')
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        registration_response = self.client.post(path=url, data=data)
        user_token = registration_response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user_token)

    def test_change_password_successfully(self):
        data = {
            'old_password': 'dummypassword',
            'new_password1': 'newdummypassword',
            'new_password2': 'newdummypassword'
        }
        response = self.client.post(path=self.password_change_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_password_with_mismatch_passwords(self):
        data = {
            'old_password': 'dummypassword',
            'new_password1': 'newdummypassword',
            'new_password2': 'newdummypasswordzzz'
        }
        response = self.client.post(path=self.password_change_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_of_user_with_social_account(self):
        registered_user = APIUser.objects.first()
        SocialAccount.objects.create(user=registered_user,
                                     provider='Social Account',
                                     uid='randomUID')
        url = reverse('password-change')
        data = {
            'old_password': 'dummmypassword',
            'new_password1': 'newdummypassword',
            'new_password2': 'newdummypasswordzzz'
        }
        response = self.client.post(path=url, data=data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'message': 'You cannot change your password.'})


class LoginTests(APITestCase):

    login_url = reverse('login')

    def setUp(self):
        url = reverse('register-account')
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password1': 'dummypassword',
            'password2': 'dummypassword'
        }
        self.client.post(path=url, data=data)

    def test_successful_login_should_return_user_data_and_token(self):
        registered_user = APIUser.objects.first()
        data = {
            'username': 'dummy_username',
            'email': 'dummyemail@mail.net',
            'password': 'dummypassword'
        }
        response = self.client.post(path=self.login_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('token', response.data.keys())
        self.assertIn('user', response.data.keys())

        user_data = response.data.get('user')

        self.assertEqual(user_data.get('pk'), registered_user.id)
        self.assertEqual(user_data.get('username'), registered_user.username)
        self.assertEqual(user_data.get('email'), registered_user.email)
        self.assertEqual(user_data.get('first_name'), registered_user.first_name)
        self.assertEqual(user_data.get('last_name'), registered_user.last_name)

    def test_login_with_nonregistered_account_should_400(self):
        data = {
            'username': 'usernamezz',
            'email': 'emailzz@mail.net',
            'password': 'secretzz'
        }
        response = self.client.post(path=self.login_url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
