from allauth.account.views import ConfirmEmailView
from allauth.socialaccount.views import signup as social_signup
from django.urls import include, path
from knox.views import LogoutAllView, LogoutView

from api_auth.views import (
    AccountPasswordChangeView, GoogleLoginView, LinkedInOAuth2LoginView,
    LoginView, RegisterView, SendEmailVerificationView,
    linkedin_login_callback, null_view, registration_complete_view)

urlpatterns = [
    path('auth/login/', LoginView.as_view(), name='login'),
    path('auth/logout/', LogoutView.as_view(), name='logout'),
    path('auth/logoutall/', LogoutAllView.as_view(), name='logoutall'),
    path('auth/login/google/', GoogleLoginView.as_view(), name='google-login'),
    path('auth/login/linkedin_oauth2/', LinkedInOAuth2LoginView.as_view(), name='linkedin-oauth2-login'),
    path('auth/login/linkedin_oauth2/callback/', linkedin_login_callback, name='linkedin-oauth2-login-callback'),

    path('accounts/social/signup', social_signup, name='socialaccount_signup'),
    path('accounts/password/reset/', include('django_rest_passwordreset.urls')),
    path('accounts/password/change/', AccountPasswordChangeView.as_view(), name='password-change'),

    path('registration/', RegisterView.as_view(), name='register-account'),
    path('registration/account-email-verification/', SendEmailVerificationView.as_view(),
         name='send-email-verification'),
    path('registration/account-email-verification-sent/', null_view, name='account_email_verification_sent'),
    path('registration/account-confirm-email/<str:key>/', ConfirmEmailView.as_view(), name='account_confirm_email'),
    path('registration/complete/', registration_complete_view, name='account_confirm_complete'),
]
