import requests
from allauth.account import app_settings as allauth_settings
from allauth.account.adapter import get_adapter
from allauth.account.utils import complete_signup, send_email_confirmation
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.linkedin_oauth2.views import \
    LinkedInOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from django.conf import settings
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_auth.registration.views import RegisterView as RestAuthRegisterView
from rest_auth.views import LoginView as RestAuthLoginView
from rest_auth.views import PasswordChangeView
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from api_auth.serializers import KnoxSerializer
from api_auth.utils import create_knox_token


@api_view()
@permission_classes((AllowAny,))
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view()
@permission_classes((AllowAny,))
def registration_complete_view(request):
    return Response({'message': 'Email verified.'})


class SendEmailVerificationView(APIView):

    def post(self, request):
        if request.user.is_email_verified:
            return Response({'message': 'Email is already verified.'}, status=status.HTTP_400_BAD_REQUEST)
        send_email_confirmation(request=request, user=request.user, signup=False)
        return Response({'message': 'Email verification sent.'})


class AccountPasswordChangeView(PasswordChangeView):

    def post(self, request, *args, **kwargs):
        user = request.user
        if SocialAccount.objects.filter(user_id=user.id).exists():
            return Response({'message': 'You cannot change your password.'},
                            status=status.HTTP_403_FORBIDDEN)
        return super(AccountPasswordChangeView, self).post(request, *args, **kwargs)


class LoginView(RestAuthLoginView):

    def get_response(self):
        serializer_class = self.get_response_serializer()
        data = {'user': self.user, 'token': self.token[-1]}
        serializer = serializer_class(instance=data, context={'request': self.request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class RegisterView(RestAuthRegisterView):

    def get_response_data(self, user):
        return KnoxSerializer({'user': user, 'token': self.token[-1]}).data

    def perform_create(self, serializer):
        user = serializer.save(self.request)
        self.token = create_knox_token(None, user, None)
        complete_signup(self.request._request, user, allauth_settings.EMAIL_VERIFICATION, None)
        return user


class APISocialLoginView(LoginView):
    serializer_class = SocialLoginSerializer

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)


class GoogleLoginView(APISocialLoginView):
    adapter_class = GoogleOAuth2Adapter
    callback_url = "/"
    client_class = OAuth2Client


class LinkedInOAuth2LoginView(APISocialLoginView):
    adapter_class = LinkedInOAuth2Adapter
    callback_url = "/"
    client_class = OAuth2Client


@api_view(['GET'])
@permission_classes((AllowAny,))
def linkedin_login_callback(request):
    try:
        linkedin_token_endpoint = 'https://www.linkedin.com/oauth/v2/accessToken'
        authorization_code = request.GET.get('code', '')
        token_endpoint_response = requests.post(
            url=linkedin_token_endpoint,
            data={
                'code': authorization_code,
                'client_id': settings.LINKEDIN_OAUTH2_CLIENT_ID,
                'client_secret': settings.LINKEDIN_OAUTH2_CLIENT_SECRET,
                'redirect_uri': settings.LINKEDIN_OAUTH2_REDIRECT_URL,
                'grant_type': 'authorization_code',
            }).json()
        login_data = {'code': authorization_code, 'access_token': token_endpoint_response['access_token']}
        login_uri = reverse('linkedin-oauth2-login', request=request)
        response = requests.post(url=login_uri, data=login_data).json()
        return Response(data=response, status=status.HTTP_200_OK)
    except KeyError:
        return Response({'Error': 'Authentication failed.'}, status=status.HTTP_403_FORBIDDEN)
