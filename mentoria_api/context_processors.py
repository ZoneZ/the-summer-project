from django.conf import settings


def from_settings(request):
    return {
        'ENVIRONMENT_NAME': settings.ADMIN_ENVIRONMENT_NAME,
        'ENVIRONMENT_COLOR': settings.ADMIN_ENVIRONMENT_COLOR,
    }
