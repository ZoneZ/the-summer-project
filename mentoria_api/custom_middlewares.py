from django.utils.deprecation import MiddlewareMixin


class HTTPXForwardedForMiddleWare(MiddlewareMixin):

    def process_request(self, request):
        if 'HTTP_X_FORWARDED_FOR' in request.META and request.META['HTTP_X_FORWARDED_FOR']:
            if 'REMOTE_ADDR' not in request.META or not request.META['REMOTE_ADDR']:
                request.META['REMOTE_ADDR'] = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
