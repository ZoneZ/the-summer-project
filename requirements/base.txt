asn1crypto==0.24.0
boto3==1.9.222
botocore==1.12.222
cachetools==3.1.1
certifi==2019.6.16
cffi==1.12.3
chardet==3.0.4
cryptography==2.7
defusedxml==0.6.0
Django==2.2.5
django-allauth==0.40.0
django-ckeditor==5.7.1
django-cors-headers==3.1.0
django-js-asset==1.2.2
django-rest-auth==0.9.5
django-rest-knox==4.1.0
django-rest-passwordreset==1.0.0
django-storages==1.7.1
django-summernote==0.8.11.4
djangorestframework==3.10.3
djangorestframework-simplejwt==4.3.0
docutils==0.15.2
future==0.17.1
httplib2==0.13.1
idna==2.8
jmespath==0.9.4
oauthlib==3.1.0
Pillow==6.1.0
psycopg2==2.8.3
pyasn1==0.4.7
pyasn1-modules==0.2.6
pycparser==2.19
PyJWT==1.7.1
python-dateutil==2.8.0
python-dotenv==0.10.3
python3-openid==3.1.0
pytz==2019.2
requests==2.22.0
requests-oauthlib==1.2.0
rsa==4.0
six==1.12.0
s3transfer==0.2.1
sqlparse==0.3.0
uritemplate==3.0.0
urllib3==1.25.3